﻿
{*****************************************************************************}
{                                                                             }
{                 _   _                  _____                                }
{                | \ | |                / ____|                               }
{                |  \| |   __ _   ____ | |        ___                         }
{                | . ` |  / _` | |_  / | |       / _ \                        }
{                | |\  | | (_| |  / /  | |____  | (_) |                       }
{                |_| \_|  \__,_| /___|  \_____|  \___/                        }
{                                                                             }
{                                                                             }
{                                                                             }
{   Author    : Nazar Chaikovskyi <nazar1986@gmail.com>                       }
{                                                                             }
{                 Copyright(c) 2016 Nazar Chaikovskyi                         }
{                                                                             }
{*****************************************************************************}


unit NazCo.Logs;

interface

uses
  SysUtils, Rtti;

type
  {$REGION 'types'}
  TLogKind = (
    lkEnter,
    lkLeave,
    lkInfo,
    lkWarning,
    lkError,
    lkException,
    lkValue,
    lkDebug);
    {$ENDREGION}


  {$REGION 'TLogEntry'}
  TLogEntry = record
  private
    FKind       : TLogKind;
    FMessage    : String;
    FTimeStamp  : TDateTime;
    FException  : Exception;
  public
    constructor Create(AKind: TLogKind; const AMessage: String); overload;
    constructor Create(AKind: TLogKind; const AMessage: String; const E: Exception); overload;

    function SetException(const E: Exception): TLogEntry;

    property Kind: TLogKind read FKind;
    property Msg: String read FMessage;
    property TimeStamp: TDateTime read FTimeStamp;
    property Exception: Exception read FException;
  end;
  {$ENDREGION}

  {$REGION 'ILog'}
  {$M+}
  ILog = interface
    ['{82489980-29C9-43D2-A02B-4C4776ABC401}']

    procedure Log(const AEntry: TLogEntry);

    procedure Enter(const AName: String);
    procedure Leave(const AName: String);

    procedure Info(const AMessage: String);
    procedure Debug(const AMessage: String);
    procedure Warning(const AMessage: String);
    procedure Error(const AMessage: String);
    procedure Exception(const AMessage: String; AException: Exception = nil);
//    procedure Value(const AName: String; const AValue: TValue);
  end;
  {$ENDREGION}

  {$REGION 'ILogger'}
  ILogger = interface
    ['{9C915782-E70A-4CB5-9ED2-071674C50355}']

    procedure RegisterLog(const ALog: ILog);
    procedure UnRegisterLog(const ALog: ILog);
  end;
  {$ENDREGION}

  {$REGION 'TLogBase'}
  TLogBase = class abstract(TInterfacedObject, ILog)
  protected
    function  GetLogKindStr(AKind: TLogKind): String;

    procedure DoLog(const AEntry: TLogEntry); virtual; abstract;
  public
    procedure Log(const AEntry: TLogEntry);

    procedure Enter(const AName: String);
    procedure Leave(const AName: String);

    procedure Info(const AMessage: String);
    procedure Debug(const AMessage: String);
    procedure Warning(const AMessage: String);
    procedure Error(const AMessage: String);
    procedure Exception(const AMessage: String; AException: Exception = nil);
  end;
  {$ENDREGION}


var
  Logger : ILog = nil;

implementation


{$REGION 'TLogEntry'}

{ TLogEntry }

//---------------------------------------------------------------------------
constructor TLogEntry.Create(AKind: TLogKind; const AMessage: String);
begin
  FKind       := AKind;
  FMessage    := AMessage;
  FTimeStamp  := Now();
  FException  := nil;
end;

//---------------------------------------------------------------------------
constructor TLogEntry.Create(AKind: TLogKind; const AMessage: String; const E: Exception);
begin
  Create(AKind, AMessage);
  FException := E;
end;

//---------------------------------------------------------------------------
function TLogEntry.SetException(const E: Exception): TLogEntry;
begin
  Result := Self;
  Result.FException := E;
end;

{$ENDREGION}

{$REGION 'TLogBase'}

{ TLogBase }

//---------------------------------------------------------------------------
procedure TLogBase.Debug(const AMessage: String);
begin
  Log(TLogEntry.Create(TLogKind.lkDebug, AMessage));
end;

//---------------------------------------------------------------------------
procedure TLogBase.Enter(const AName: String);
begin
  Log(TLogEntry.Create(TLogKind.lkEnter, AName));
end;

//---------------------------------------------------------------------------
procedure TLogBase.Error(const AMessage: String);
begin
  Log(TLogEntry.Create(TLogKind.lkError, AMessage));
end;

//---------------------------------------------------------------------------
procedure TLogBase.Exception(const AMessage: String; AException: Exception);
begin
  Log(TLogEntry.Create(TLogKind.lkException, AMessage, AException));
end;

//---------------------------------------------------------------------------
function TLogBase.GetLogKindStr(AKind: TLogKind): String;
begin
  case AKind of
    lkEnter     : Result := '[ENTER]';
    lkLeave     : Result := '[LEAVE]';
    lkInfo      : Result := '[INFO ]';
    lkWarning   : Result := '[WARNG]';
    lkError     : Result := '[ERROR]';
    lkException : Result := '[EXCEP]';
    lkValue     : Result := '[VALUE]';
    lkDebug     : Result := '[DEBUG]';
  else Result := '';
  end;
end;

//---------------------------------------------------------------------------
procedure TLogBase.Info(const AMessage: String);
begin
  Log(TLogEntry.Create(TLogKind.lkInfo, AMessage));
end;

//---------------------------------------------------------------------------
procedure TLogBase.Leave(const AName: String);
begin
  Log(TLogEntry.Create(TLogKind.lkLeave, AName));
end;

//---------------------------------------------------------------------------
procedure TLogBase.Log(const AEntry: TLogEntry);
begin
  DoLog(AEntry);
end;

//---------------------------------------------------------------------------
procedure TLogBase.Warning(const AMessage: String);
begin
  Log(TLogEntry.Create(TLogKind.lkWarning, AMessage));
end;

{$ENDREGION}

end.

