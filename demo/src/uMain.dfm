object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'fmMain'
  ClientHeight = 337
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblTime1000Blocks: TLabel
    Left = 239
    Top = 77
    Width = 85
    Height = 13
    Caption = 'lblTime1000Blocks'
  end
  object lblTime10000: TLabel
    Left = 239
    Top = 117
    Width = 62
    Height = 13
    Caption = 'lblTime10000'
  end
  object lblTime100000: TLabel
    Left = 239
    Top = 156
    Width = 68
    Height = 13
    Caption = 'lblTime100000'
  end
  object lblTime1000000: TLabel
    Left = 239
    Top = 197
    Width = 74
    Height = 13
    Caption = 'lblTime1000000'
  end
  object lblTimeBlock: TLabel
    Left = 239
    Top = 37
    Width = 56
    Height = 13
    Caption = 'lblTimeBlock'
  end
  object btnLog1000Blocks: TButton
    Left = 24
    Top = 72
    Width = 209
    Height = 25
    Caption = 'btnLog1000Blocks'
    TabOrder = 0
    OnClick = btnLog1000BlocksClick
  end
  object btnLog10000: TButton
    Left = 24
    Top = 112
    Width = 209
    Height = 25
    Caption = 'btnLog10000'
    TabOrder = 1
    OnClick = btnLog10000Click
  end
  object btnLog100000: TButton
    Left = 24
    Top = 151
    Width = 209
    Height = 25
    Caption = 'btnLog100000'
    TabOrder = 2
    OnClick = btnLog100000Click
  end
  object btnLog1000000: TButton
    Left = 24
    Top = 192
    Width = 209
    Height = 25
    Caption = 'btnLog1000000'
    TabOrder = 3
    OnClick = btnLog1000000Click
  end
  object btnLogBlock: TButton
    Left = 24
    Top = 32
    Width = 209
    Height = 25
    Caption = 'btnLogBlock'
    TabOrder = 4
    OnClick = btnLogBlockClick
  end
end
