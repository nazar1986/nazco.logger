program Logger;

uses
  Vcl.Forms,
  uMain in 'src\uMain.pas' {fmMain},
  NazCo.Logs in '..\source\NazCo.Logs.pas',
  NazCo.Logs.FileStream in '..\source\NazCo.Logs.FileStream.pas',
  NazCo.Logs.Logger in '..\source\NazCo.Logs.Logger.pas',
  NazCo.Logs.CodeSite in '..\source\NazCo.Logs.CodeSite.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
