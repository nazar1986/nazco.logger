﻿
{*****************************************************************************}
{                                                                             }
{                 _   _                  _____                                }
{                | \ | |                / ____|                               }
{                |  \| |   __ _   ____ | |        ___                         }
{                | . ` |  / _` | |_  / | |       / _ \                        }
{                | |\  | | (_| |  / /  | |____  | (_) |                       }
{                |_| \_|  \__,_| /___|  \_____|  \___/                        }
{                                                                             }
{                                                                             }
{                                                                             }
{   Author    : Nazar Chaikovskyi <nazar1986@gmail.com>                       }
{                                                                             }
{                 Copyright(c) 2016 Nazar Chaikovskyi                         }
{                                                                             }
{*****************************************************************************}


unit NazCo.Logs.FileStream;

interface

uses
  NazCo.Logs, Classes, SysUtils, SyncObjs, Windows;


type
  {$REGION 'TLogFileStream'}
  TLogFileStream = class(TLogBase)
  strict private
    FStream     : TFileStream;
    FEncoding   : TEncoding;
    FLock       : TCriticalSection;
  private
    function FormatTimeStamp(const ATimeStamp: TDateTime): String; inline;
  protected
    procedure DoLog(const AEntry: TLogEntry); override;
  public
    constructor Create(AFileName: String);
    destructor  Destroy(); override;
  end;
  {$ENDREGION}


implementation

{$REGION 'TLogFileStream implementation'}
{ TLogFileStream }

//---------------------------------------------------------------------------
constructor TLogFileStream.Create(AFileName: String);
var
  iFileHandle : Integer;
begin
  inherited Create();

  if (not FileExists(AFileName)) then
  begin
    iFileHandle := FileCreate(AFileName);
    if (iFileHandle = -1) then
      raise EFCreateError.CreateFmt('Cannot create file "%s". %s', [ExpandFileName(AFileName), SysErrorMessage(GetLastError)]);
    CloseHandle(iFileHandle);
  end;

  FStream := TFileStream.Create(AFileName, fmOpenWrite or fmShareDenyNone, 0);
  FStream.Position := FStream.Size;

  FLock     := TCriticalSection.Create();
  FEncoding := TEncoding.UTF8;
end;

//---------------------------------------------------------------------------
destructor TLogFileStream.Destroy();
begin
  FreeAndNil(FStream);
  FreeAndNil(FLock);

  inherited;
end;

//---------------------------------------------------------------------------
procedure TLogFileStream.DoLog(const AEntry: TLogEntry);
var
  vBuffer : TBytes;
begin
  vBuffer := FEncoding.GetBytes(FormatTimeStamp(AEntry.TimeStamp) + ' ' +
                GetLogKindStr(AEntry.Kind) + ' - ' + AEntry.Msg + sLineBreak);
  FLock.Enter;
  try
    fStream.WriteBuffer(vBuffer[0], Length(vBuffer));
  finally
    FLock.Leave;
  end;
end;

//---------------------------------------------------------------------------
function TLogFileStream.FormatTimeStamp(const ATimeStamp: TDateTime): String;
begin
  Result := FormatDateTime('dd.mm.yyyy hh:nn:ss:zzz', ATimeStamp);
end;
{$ENDREGION}

end.

