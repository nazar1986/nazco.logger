# README #

### How to use logger ###

**Init Logger**


```
#!pascal

Logger := TLogger.Create();
(Logger as TLogger).RegisterLog(TLogFileStream.Create(FILE_PATH));
(Logger as TLogger).RegisterLog(TLogCodeSite.Create());

```
