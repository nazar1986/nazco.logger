unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls;

type
  TfmMain = class(TForm)
    btnLog1000Blocks: TButton;
    lblTime1000Blocks: TLabel;
    btnLog10000: TButton;
    btnLog100000: TButton;
    btnLog1000000: TButton;
    lblTime10000: TLabel;
    lblTime100000: TLabel;
    lblTime1000000: TLabel;
    btnLogBlock: TButton;
    lblTimeBlock: TLabel;
    procedure btnLog1000BlocksClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLog10000Click(Sender: TObject);
    procedure btnLog100000Click(Sender: TObject);
    procedure btnLog1000000Click(Sender: TObject);
    procedure btnLogBlockClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitLogger();

    procedure ProcessLogBlock(ABlockIdx: Integer);
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;

implementation

uses
  Diagnostics, NazCo.Logs, NazCo.Logs.FileStream, NazCo.Logs.Logger, NazCo.Logs.CodeSite;

{$R *.dfm}

//---------------------------------------------------------------------------
procedure TfmMain.btnLog1000000Click(Sender: TObject);
var
  i       : Integer;
  vTimer  : TStopwatch;
begin
  vTimer := TStopwatch.Create();
  try
    vTimer.Start();

    for i := 1 to 1000000 do
    begin
      Logger.Info('Info Message');
    end;
  finally
    vTimer.Stop();
    lblTime1000000.Caption := Format('Time spent: [%d]', [vTimer.ElapsedMilliseconds]);
  end;
end;

//---------------------------------------------------------------------------
procedure TfmMain.btnLog100000Click(Sender: TObject);
var
  i       : Integer;
  vTimer  : TStopwatch;
begin
  vTimer := TStopwatch.Create();
  try
    vTimer.Start();

    for i := 1 to 100000 do
    begin
      Logger.Info('Info Message');
    end;
  finally
    vTimer.Stop();
    lblTime100000.Caption := Format('Time spent: [%d]', [vTimer.ElapsedMilliseconds]);
  end;
end;

//---------------------------------------------------------------------------
procedure TfmMain.btnLog10000Click(Sender: TObject);
var
  i       : Integer;
  vTimer  : TStopwatch;
begin
  vTimer := TStopwatch.Create();
  try
    vTimer.Start();

    for i := 1 to 10000 do
    begin
      Logger.Info('Info Message');
    end;
  finally
    vTimer.Stop();
    lblTime10000.Caption := Format('Time spent: [%d]', [vTimer.ElapsedMilliseconds]);
  end;
end;

//---------------------------------------------------------------------------
procedure TfmMain.btnLog1000BlocksClick(Sender: TObject);
var
  i: Integer;
  vTimer  : TStopwatch;
begin
  vTimer := TStopwatch.Create();
  try
    vTimer.Start();

    for i := 1 to 1000 do
    begin
      ProcessLogBlock(i);
    end;
  finally
    vTimer.Stop();
    lblTime1000Blocks.Caption := Format('Time spent: [%d]', [vTimer.ElapsedMilliseconds]);
  end;
end;

//---------------------------------------------------------------------------
procedure TfmMain.btnLogBlockClick(Sender: TObject);
var
  vTimer  : TStopwatch;
begin
  vTimer := TStopwatch.Create();
  try
    vTimer.Start();

    ProcessLogBlock(0);
  finally
    vTimer.Stop();
    lblTimeBlock.Caption := Format('Time spent: [%d]', [vTimer.ElapsedMilliseconds]);
  end;
end;

//---------------------------------------------------------------------------
procedure TfmMain.FormCreate(Sender: TObject);
begin
  InitLogger();
end;

//---------------------------------------------------------------------------
procedure TfmMain.InitLogger();
begin
  Logger := TLogger.Create();
  (Logger as TLogger).RegisterLog(TLogFileStream.Create('D:\logger.log'));
  (Logger as TLogger).RegisterLog(TLogCodeSite.Create());
end;

//---------------------------------------------------------------------------
procedure TfmMain.ProcessLogBlock(ABlockIdx: Integer);
begin
  Logger.Enter(Format('Enter loop [%d]', [ABlockIdx]));
  try
    Logger.Info('Info Message');
    Logger.Debug('Debug Message');
    Logger.Warning('Warning Message');
    Logger.Error('Error Message');
    Logger.Exception('Exception Message', Exception.Create('Exception Message'));
  finally
    Logger.Leave(Format('Leave loop [%d]', [ABlockIdx]));
  end;
end;

end.
