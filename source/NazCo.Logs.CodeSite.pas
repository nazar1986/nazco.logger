﻿
{*****************************************************************************}
{                                                                             }
{                 _   _                  _____                                }
{                | \ | |                / ____|                               }
{                |  \| |   __ _   ____ | |        ___                         }
{                | . ` |  / _` | |_  / | |       / _ \                        }
{                | |\  | | (_| |  / /  | |____  | (_) |                       }
{                |_| \_|  \__,_| /___|  \_____|  \___/                        }
{                                                                             }
{                                                                             }
{                                                                             }
{   Author    : Nazar Chaikovskyi <nazar1986@gmail.com>                       }
{                                                                             }
{                 Copyright(c) 2016 Nazar Chaikovskyi                         }
{                                                                             }
{*****************************************************************************}


unit NazCo.Logs.CodeSite;

interface

uses
  NazCo.Logs;


type
  {$REGION 'TLogCodeSite'}
  TLogCodeSite = class(TLogBase)
  protected
    procedure DoLog(const AEntry: TLogEntry); override;
  public

  end;
  {$ENDREGION}


implementation

uses
  CodeSiteLogging;

{$REGION 'TLogCodeSite implementation'}
{ TLogFileStream }

//---------------------------------------------------------------------------
procedure TLogCodeSite.DoLog(const AEntry: TLogEntry);
begin
  case AEntry.Kind of
    lkEnter:
    begin
      CodeSite.EnterMethod(AEntry.Msg);
    end;
    lkLeave:
    begin
      CodeSite.ExitMethod(AEntry.Msg);
    end;
    lkInfo:
    begin
      CodeSite.Send(AEntry.Msg);
    end;
    lkWarning:
    begin
      CodeSite.SendWarning(AEntry.Msg);
    end;
    lkError:
    begin
      CodeSite.SendError(AEntry.Msg);
    end;
    lkException:
    begin
      if (Assigned(AEntry.Exception)) then
        CodeSite.SendException(AEntry.Msg, AEntry.Exception)
      else
        CodeSite.SendError(AEntry.Msg);
    end;
    lkDebug:
    begin
      CodeSite.Send(AEntry.Msg);
    end;
  end;
end;
{$ENDREGION}

end.

