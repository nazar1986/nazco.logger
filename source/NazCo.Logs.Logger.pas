﻿
{*****************************************************************************}
{                                                                             }
{                 _   _                  _____                                }
{                | \ | |                / ____|                               }
{                |  \| |   __ _   ____ | |        ___                         }
{                | . ` |  / _` | |_  / | |       / _ \                        }
{                | |\  | | (_| |  / /  | |____  | (_) |                       }
{                |_| \_|  \__,_| /___|  \_____|  \___/                        }
{                                                                             }
{                                                                             }
{                                                                             }
{   Author    : Nazar Chaikovskyi <nazar1986@gmail.com>                       }
{                                                                             }
{                 Copyright(c) 2016 Nazar Chaikovskyi                         }
{                                                                             }
{*****************************************************************************}


unit NazCo.Logs.Logger;

interface

uses
  NazCo.Logs, Generics.Collections, SysUtils;


type
  {$REGION 'TLogger'}
  TLogger = class(TLogBase, ILogger)
  private
    FLogs : TList<ILog>;
  protected
    procedure DoLog(const AEntry: TLogEntry); override;
  public
    constructor Create();
    destructor  Destroy; override;

    procedure RegisterLog(const ALog: ILog);
    procedure UnRegisterLog(const ALog: ILog);
  end;
  {$ENDREGION}

implementation


{$REGION 'TLogger implementation'}
{ TLogger }

//---------------------------------------------------------------------------
constructor TLogger.Create();
begin
  FLogs := TList<ILog>.Create();
end;

//---------------------------------------------------------------------------
destructor TLogger.Destroy();
begin
  FreeAndNil(FLogs);
  inherited;
end;

//---------------------------------------------------------------------------
procedure TLogger.DoLog(const AEntry: TLogEntry);
var
  LLog: ILog;
begin
  for LLog in FLogs do
    LLog.Log(AEntry);
end;

//---------------------------------------------------------------------------
procedure TLogger.RegisterLog(const ALog: ILog);
begin
  if (Assigned(ALog)) then
    FLogs.Add(ALog);
end;

//---------------------------------------------------------------------------
procedure TLogger.UnRegisterLog(const ALog: ILog);
begin
  if (Assigned(ALog)) then
    FLogs.Remove(ALog);
end;
{$ENDREGION}

end.

